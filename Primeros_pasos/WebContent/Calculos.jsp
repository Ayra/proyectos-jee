<%@ page import="com.pe.oscar.primerpaquete.Calculos" %>

<html>

	<body>
	
	
		<h1>Modularizaacion de las clases</h1>
		
		<b>Suma de 9 y 7 es : <%= Calculos.metodoSuma(9, 7) %></b>
		<br>
		<b>Resta de 9 y 7 es : <%= Calculos.metodoResta(9, 7) %></b>
		<br>
		<b>Multiplicacion de 9 y 7 es : <%= Calculos.metodoMultiplica(9, 7) %></b>
		
	</body>


</html>
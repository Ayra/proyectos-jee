package com.pildorasinformaticas.productos;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.*;

import com.mysql.jdbc.Statement;

/**
 * Servlet implementation class ServletPruebas
 */
@WebServlet("/ServletPruebas")
public class ServletPruebas extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	//Definir o establecer el DataSource
	
	@Resource(name="jdbc/Usuarios")
	private DataSource miPool;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletPruebas() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		//crear el obj printWritter
		
		PrintWriter salida = response.getWriter();
		
		response.setContentType("text/plain");
		
		//crar conexion con BBDD
		
		Connection miConexion = null;
		Statement miStatement = null;
		ResultSet miResultSet = null;
		
		try {
			
			miConexion = miPool.getConnection();
			String miSql = "SELECT * FROM USUARIOS";
			miStatement = miConexion.createStatement();
			miResultSet = miStatement.executeQuery(miSql);
			
			while (miResultSet.next()) {
				
				String nombre_persona = miResultSet.getString(2);
				salida.println("nombre del usuario: "+ nombre_persona);
			}
			
		} catch (Exception e) {
			e.getStackTrace();
			salida.println("Error:"+ e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

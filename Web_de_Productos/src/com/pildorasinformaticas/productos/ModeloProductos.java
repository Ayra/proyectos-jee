package com.pildorasinformaticas.productos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

public class ModeloProductos {

	private DataSource origenDatos;
	
	public ModeloProductos(DataSource origenDatos) {
		
		this.origenDatos = origenDatos;
	}
	
	public List<Productos> getProductos() throws Exception{
	
		List<Productos> productos = new ArrayList<>();
		
		Connection miConexion = null;
		Statement miStatement = null;
		ResultSet miResultSet = null;
		
		/*		Establecer conexion		*/
		
		miConexion = origenDatos.getConnection();
		
		/*		Crear sentencia SQL y Statement 	*/
		
		String sentencia_sql = "SELECT * FROM PRODUCTOS";
		miStatement = miConexion.createStatement();
		
		/*		Ejecutar SQL			*/
		
		miResultSet = miStatement.executeQuery(sentencia_sql);
		
		/*		Recorrer el resultSet obtenido		*/
		
		while (miResultSet.next()) {
			
			int	s_art = miResultSet.getInt("CODIGOARTICULO");
			String	seccion = miResultSet.getString("SECCION");
			String n_art = miResultSet.getString("NOMBREARTICULO");
			double precio = miResultSet.getDouble("PRECIO");
			Date fecha = miResultSet.getDate("FECHA");
			String importado = miResultSet.getString("IMPORTADO");
			String p_orig = miResultSet.getString("PAISDEORIGEN");
			
			Productos tempProd = new Productos(s_art, seccion, n_art, precio, fecha, importado, p_orig);
			
			productos.add(tempProd);
		}
		
		return productos;
	}
}

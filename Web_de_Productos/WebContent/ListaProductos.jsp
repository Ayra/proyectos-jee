<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="java.util.*"%>
<%@ page import="com.pildorasinformaticas.productos.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
</head>
<body>

	<%
		/*	Obtiene la lista de productos del servlet	*/
		List<Productos> losProductos = (List<Productos>) request.getAttribute("LISTAPRODUCTOS");
	%>

	<table class="table table-striped col-md-9">
		<tr>
			<th scope="col">CODIGO DE ARTICULO</td>
			<th scope="col">SECCION</td>
			<th scope="col">NOMBRE DE ARTICULO</td>
			<th scope="col">FECHA</td>
			<th scope="col">PRECIO</td>
			<th scope="col">IMPORTADO</td>
			<th scope="col">PAIS DE ORIGEN</td>
		</tr>

		<%
			for (Productos tempProducto : losProductos) {
		%>

		<tr>
			<td><%=tempProducto.getcArt()%></td>
			<td><%=tempProducto.getSeccion()%></td>
			<td><%=tempProducto.getnArt()%></td>
			<td><%=tempProducto.getFecha()%></td>
			<td><%=tempProducto.getPrecio()%></td>
			<td><%=tempProducto.getImportado()%></td>
			<td><%=tempProducto.getpOrig()%></td>
		</tr>

		<%
			}
		%>
	</table>

</body>
</html>